---
title: Jailbreak 2020-04-24 - Présentation de Jailbreak au Pôle Systematic
separator: <!--s-->
verticalSeparator: <!--v-->
theme: simple
revealOptions:
    transition: 'slide'
---

![](./assets/jailbreak.png)

<!--s-->

# 4 fondateurs

- Paula Forteza
- Christophe Benz
- Emmanuel Raviart
- Johan Richer

### associés en SAS

<!--s-->

# Historique

- Entreprenariat, administration, associations, international...
- Easter-eggs, Entr'ouvert, Libre entreprise...
- Déclic à Etalab en 2016
- Lancement début 2017

<!--s-->

## 2 salariés

- Bruno Duyé
- Pierre Dittgen

nous ont depuis rejoint

<!--s-->

![](./assets/team.jpg)

<!--s-->

![](./assets/valeurs.png)

<!--s-->

![](./assets/projets.png)

<!--v-->

![](./assets/DBnomics.png)

<!--s-->

## Pari sur des _workhorses_ open source

- Gitlab, Gitlab-CI
- Docker
- Python (Pandas, Plotly, Jupyter...)
- Javascript (Sapper, Svelte...)
- Discourse
- Wikibase

<!--s-->

## ... et open data

- OpenStreetMap
- Wikidata

<!--s-->

## Notre modèle

- Beaucoup de service
- Un peu de conseil, formation

<!--s-->

## Les données
et leur traitement de bout en bout
- Collecte, production
- Transformation
- Publication
- Exploitation, valorisation

<!--s-->

## L'open data
Quel avenir ?

On a peut-être notre mot à dire...

<!--s-->

## Avenir

- Edition ?
- Partenariats
- Ouverture à l'international

<!--s-->

## Notre communauté

- Réseau Libre entreprise (Easter-eggs, Entr'ouvert...)
- April
- Code for France
- TeamOpenData.org
- Open Knowledge, Frictionless Data, Datopian
- OpenStreetMap
- Wikimedia

<!--v-->

![](./assets/codeforfrance.jpg)

<!--v-->

![](./assets/frictionlessdata.png)

<!--v-->

![](./assets/frictionlessdata_blog.png)

<!--s-->

[<img src="./assets/jailbreak-logo.png" width="30%">](https://jailbreak.paris/)

**Johan Richer**  
Directeur général  
[twitter.com/JohanRicher](https://twitter.com/JohanRicher)

### Merci !
