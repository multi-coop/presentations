---
title: Jailbreak 2021-10-05 - Les données territoriales au service de l’action publique
separator: <!--s-->
verticalSeparator: <!--v-->
theme: assets/simple-custom.css
revealOptions:
    transition: 'fade'
---

## L'_open source_ au service<br>de l'_open data_ des territoires

L'enjeu de la qualité des données

<img src="./assets/systematic-logo.png" width="23%">

<img src="./assets/jailbreak-logo.png" width="30%">

##### [**Les données territoriales au service de l’action publique**](https://systematic-paris-region.org/evenement/les-donnees-territoriales-au-service-de-laction-publique/)
##### 5 octobre 2021

<!--v-->

###### [Présentation accessible en ligne](https://jailbreak.gitlab.io/presentations/2021-10-05_Open_Data_Systematic.html) ([code source](https://gitlab.com/jailbreak/presentations/))

**Publication**  
4 octobre 2021  
**Mise à jour**  
5 octobre 2021

###### **Licence :**<br>Creative Commons<br>Attribution - Partage dans les mêmes conditions<br>4.0 International<br>(CC BY-SA 4.0)

<!--s-->

<img src="./assets/jailbreak-logo.png" width="30%">

* Entreprise créée en 2017
<br>par des anciens d'[Etalab](https://etalab.gouv.fr/) et d'[Easter-eggs](https://www.easter-eggs.com/)
* _« Des logiciels libres pour des données libres »_

<!--s-->

<img src="./assets/validata-logo.png" width="20%">

* La **qualité** : un enjeu majeur de l'_open data_
* Un projet porté par [OpenDataFrance](https://opendatafrance.net)
* Développé par [Jailbreak](https://jailbreak.paris/) depuis 2018
* Des spécifications : les **schémas**
* Un outil : le **validateur**

<!--s-->

## Les schémas de données

Décrivent la structure d'un fichier tabulaire (CSV, Excel) :

* quelles sont les différentes colonnes
* quelle type de données dans chaque colonne
* quelles sont les valeurs possibles

<!--v-->

Exemple de description d'une colonne :

```
{
    "name":"id",
    "description":"Un identifiant unique de l'objet.",
    "example":"42",
    "type":"integer",
    "constraints":{
       "required":true,
       "minimum":1
}
```

![](./assets/schema.png)

<!--s-->

## 2 objectifs

Aider les **producteurs** à créer et<br>à améliorer la qualité des données qu'ils publient

Faciliter l'exploitation des données par les **réutilisateurs**<br>(agrégation, consolidation et traitements automatiques)

<!--s-->

## Créés _pour_ et _par_ les territoires

[![](./assets/scdl.png)](https://scdl.opendatafrance.net/)

<!--s-->

## Un outil de validation

[![](./assets/validata-screenshot.png)](https://go.validata.fr/)

<!--s-->

## Un catalogue de schémas officiel

[![](./assets/schemadatagouv.png)](https://schema.data.gouv.fr/)

<!--s-->

## Un outil pour créer facilement<br>des données valides

[![](./assets/publierdatagouv.png)](https://publier.etalab.studio/)

<!--s-->

## Une communauté internationale<br>Des spécifications & outils _open source_

[![](./assets/frictionlessdata.png)](https://frictionlessdata.io/)

<!--s-->

## Conclusion

* Des outils accessibles aux collectivités 🛠️
* Pour améliorer efficacement la qualité des données ✨
* _Open Data_ 🤝 _Open Source_ 
* Communauté internationale 🌐 Usage local 🏞️
* Soutien technique et financier de l'Etat 🇫🇷


<!--s-->

[<img src="./assets/jailbreak-logo.png" width="30%">](https://jailbreak.paris/)

**Johan Richer**  
Directeur général  
[twitter.com/JohanRicher](https://twitter.com/JohanRicher)  
[johan.richer@jailbreak.paris](mailto:johan.richer@jailbreak.paris)

### Merci !